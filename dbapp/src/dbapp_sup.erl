%%%-------------------------------------------------------------------
%% @doc dbapp top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(dbapp_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).



%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
	SupFlags = #{strategy => one_for_one, 
				 intensity => 2,
				 period => 5},
	ChildSpec =  get_child_spec(),
	{ok, { SupFlags, ChildSpec}}.

%%====================================================================
%% Internal functions
%%====================================================================
get_child_spec() ->
	[#{	id => db_server1,
			start => {db_server, start_link, [db_server, [], []]},
			restart => transient,
			shutdown => brutal_kill
	},#{id => db_server2,
		start => {db_server, start_link, [db_server, [], []]},
		restart => transient,
		shutdown => brutal_kill}].