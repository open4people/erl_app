-module(db_server).
-behavior(gen_server).

-export([
	init/1,
	start_link/0,
	start_link/3,
	handle_call/3
]).

start_link() ->
	gen_server:start_link(?MODULE, [], []).

start_link(Module, Args, Opt) ->
	gen_server:start_link(Module, Args, Opt).

init([]) ->
	{ok, []}.

handle_call({write, {K,V}}, _From, State) ->
	NewState = [{K,V} | State],
	{reply, ok, NewState};

handle_call({delete, Key}, _From, State) ->
	NewState = [{K,V} || {K,V} <- State, K =/= Key],
	{reply, ok, NewState};

handle_call({read, Key}, _From, State) ->
	Res = [V || {K,V} <- State, K == Key],
	case Res of
		[] -> {reply, {error, instance}, State};
		_Else -> {reply, {ok, Res}, State}
	end;

handle_call({match, Val}, _From, State) ->
	Res = [K || {K,V} <- State, V == Val],
	case Res of
		[] -> {reply, {error, instance}, State};
		_Else -> {reply, {ok, Res}, State}
	end.



